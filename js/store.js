class StoreController extends Requests {

    constructor() {
        super()
    }

    doGetItensStore = (cb) => {
        let type = 'GET';
        let url = 'store/items';
        this.doRequest(type, url, null, cb);
    }

    doBuyItem = (data, cb) => {
        let type = 'POST';
        let path = 'store/buy';
        this.doRequest(type, path, data, cb);
    }

    doBuyServices = (data, service, cb) => {
        let type = 'POST';
        let path = 'store/services/' + service;
        this.doRequest(type, path, data, cb);
    }
}
class Store extends StoreController {

    constructor() {
        super()
        this.services = [[0, 'Level Up', 'Evolua 10 LVLs', 15 ]];
		this.menuStoreType = ['#storeTypeWeapons', '#storeTypeArmors', '#storeTypeRep', '#storeTypeMounts', '#storeTypeProf', '#storeTypeServices' ];
        this.populateCache()
    }

    populateCache() {
        let _this = this;
        return this.doGetItensStore((err, resp) => {
            _this.cache = resp.items;
        })
    }

    getCharacters = () => {
        localUser.getCharacters((chars) => {
            chars.forEach(element => {
                if (element[1] == 1) {
                    $('#storeCharacterSelect')
                        .append($('<option>')
                            .attr('value', element[0])
                            .attr('disabled', true)
                            .text(element[0])
                        )
                } else {
                    $('#storeCharacterSelect')
                        .append($('<option>')
                            .attr('value', element[0])
                            .text(element[0])
                        )
                }
            });
        });
    }

    populateItens = (type) => {
        $('#storeContent').empty();
        //filtra os itens pelo tipo
        let filtered = this.cache.filter(function (element) {
            if (element[1] == type) {
                return element;
            }
        });

        // popula
        filtered.forEach(element => {
            $('#storeContent')
                .append($('<div>')
                    .addClass('col-md-3')
                    .addClass('outerBox')
                    .append($('<a>')
                        .attr('data-wowhead', 'item=' + element[0])
                        .append($('<div>')
                            .addClass('col-md-12')
                            .addClass('storeItemName')
                            .append($('<span>')
                                .addClass('text-center')
                                .text(element[3])
                            )
                        )
                        .append($('<div>')
                            .addClass('row')
                            .addClass('storeButton')
                            .append($('<button>')
                                .addClass('btn')
                                .addClass('btn-secondary')
                                .addClass('btn-block')
                                .text('Adicionar a Conta')
                                .click(function () { localUser.getStore().buyItem(element) })
                            )
                        )
                        .append($('<div>')
                            .addClass('row')
                            .addClass('storeContentBottom')
                            .append($('<div>')
                                .addClass('col-md-8')
                                .text(GLOBAL_ITEM_TYPES[element[1]][element[2]])
                            )
                            .append($('<div>')
                                .addClass('col-md-4')
                                .text(element[4] + ' VPs')
                            )
                        )
                    )
                )
        });
    }

    buyItem = (item) => {
        if (localUser.getVotes() < item[4]) {
            return doToastr('warning', 'VP Insuficiente', 'Você não possúi VP suficiente para comprar esse item!');
        }

        let selectedChar = $('select[name=storeCharacterSelect]').val();

        let data = { itemId: item[0], charName: selectedChar };

        this.doBuyItem(data, (err, resp) => {
            if (err) {
                return doToastr('warning', 'Error', resp.message, 5000);
            }
            return doToastr('success', 'sucesso', 'Logo o ' + selectedChar + ' receberá o item comprado!')
        });
    }

    buyServices = (index) => {
        index = 0;
        let selectedChar = $('select[name=storeCharacterSelect]').val();
        this.doBuyServices({ character: selectedChar }, this.services[index], (err) => {
            if(err){
                $('#modalDefault').modal('hide');
                switch(err.status){
                    case 200:
                        doToastr('success', 'Store', 'Serviço efetuado com sucesso!');
                        break;
                    case 403:
                        doToastr('info', 'Store', 'Seu personagem já está lvl 80!');
                        break;
                    case 400:
                    case 401:
                    case 500:
                        doToastr('warning', 'Store', 'Falha ao efetuar serviço! ' + resp.message);
                        break;
                    case 402:
                        doToastr('warning', 'Store', 'Falha ao efetuar serviço! VP Insuficiente!');
                        break;
                    default:
                        doToastr('warning', 'Store', 'Falha ao efetuar serviço! Contate um administrador');
                        break;
                }
                return;
            }
            doToastr('success', 'Store', 'Serviço efetuado com sucesso!');
        })
    }

    doModalServices = (service) => {
        let selectedChar = $('select[name=storeCharacterSelect]').val();
        $('#modalDefault').modal();
        $('#modalDefaultContent').load('components/content/vp-store/modalStore.html',() => {
            $('#storeModalMessage').text('Tem certeza que deseja adicionar 10 lvl ao personagem ' + selectedChar + '?');
            $('#storeModalForm').submit(()=> {
                localUser.getStore().buyServices(service, selectedChar);
                return false;
            })
        });
    }

    populateServiceStore = () => {
        $('#storeContent').empty();
        this.services.forEach(element => {
            $('#storeContent')
            .append($('<div>')
                .addClass('col-md-3')
                .addClass('outerBox')
                .append($('<a>')
                    .append($('<div>')
                        .addClass('col-md-12')
                        .addClass('storeItemName')
                        .append($('<span>')
                            .addClass('text-center')
                            .text(element[1])
                        )
                    )
                    .append($('<div>')
                        .addClass('row')
                        .addClass('storeButton')
                        .append($('<button>')
                            .addClass('btn')
                            .addClass('btn-secondary')
                            .addClass('btn-block')
                            .text('Adicionar ao character.')
                            .click(function() { localUser.getStore().doModalServices(element) })
                        )
                    )
                    .append($('<div>')
                        .addClass('row')
                        .addClass('storeContentBottom')
                        .append($('<div>')
                            .addClass('col-md-8')
                            .text(element[2])
                        )
                        .append($('<div>')
                            .addClass('col-md-4')
                            .text(element[3] + ' VPs')
                        )
                    )
                )
            )
        });
    }

    toggle_active_itens = (index) => {
        this.menuStoreType.forEach(itm => {
            if (itm === this.menuStoreType[index])
                return $(itm).addClass('active');
            return $(itm).removeClass('active');        
        });
    }
}