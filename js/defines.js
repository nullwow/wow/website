var GLOBAL_Provider = ['TopG', 'Top100Arena', '' ];
var GLOBAL_Gender = [ 'img/si-glyph-male.svg', 'img/si-glyph-female.svg' ];

var GLOBAL_Races = {
    1:  'Human',
    2:  'Orc',
    3:  'Dwarf',
    4:  'Night Elf',
    5:  'Undead',
    6:  'Tauren',
    7:  'Gnome',
    8:  'Troll',
    10: 'Blood Elf',
    11: 'Drainei'
};

var GLOBAL_Colors = {
    'Warrior'       : '#C79C6E', 
    'Paladin'       : '#F58CBA', 
    'Hunter'        : '#ABD473', 
    'Rogue'         : '#FFF569', 
    'Priest'        : '#FFFFFF', 
    'Deathknight'   : '#C41F3B', 
    'Shaman'        : '#0070DE', 
    'Mage'          : '#60CCF0', 
    'Warlock'       : '#9482C9', 
    'Druid'         : '#FF7D0A'
};

var GLOBAL_Class = {
    1:  'Warrior',
    2:  'Paladin',
    3:  'Hunter',
    4:  'Rogue',
    5:  'Priest',
    6:  'Deathknight',
    7:  'Shaman',
    8:  'Mage',
    9:  'Warlock',
    11: 'Druid'
};

var GLOBAL_ITEM_TYPES = {
    2: {
        0: 'One Handed Axe',
        1: 'Two Handed Axe',
        2: 'Bow',
        3: 'Gun',
        4: 'One Handed Mace',
        5: 'Two Handed Mace',
        6: 'Polearm',
        7: 'One Handed Sword',
        8: 'Two Handed Sword',
        10: 'Staff',
        13: 'Fist Weapon',
        15: 'Dagger',
        16: 'Throw',
        17: 'Spear',
        18: 'Crossbow',
        19: 'Wand',
        20: 'Fishing Pole' 
    },
    4: {
        0: 'Miscellaneous',
        1: 'Cloth',
        2: 'Leather',
        3: 'Mail',
        4: 'Plate',
        6: 'Shield',
        7: 'Libram',
        8: 'Idol',
        9: 'Totem',
        10: 'Sigil'
        },
    15: {
        5: 'Mounts'
    },
    98: {
        0: 'Common',
        1: 'Alchemy',
        2: 'Blacksmithing',
        3: 'Cooking',
        4: 'Enchanting',
        5: 'Engineering',
        6: 'Inscription',
        7: 'Jewelcrafting',
        8: 'Letherworking',
        9: 'Tailoring'
    },
    99: {
        1: 'Reputation'
    }
}